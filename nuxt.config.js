import Sass from 'sass'
import Fiber from 'fibers'

export default {
  target: 'static',

  env: {
    sitename: process.env.npm_package_name || '',
    description: process.env.npm_package_description || '',
  },

  head: {
    titleTemplate: '%s | ' + process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1, shrink-to-fit=no',
      },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || '',
      },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href: 'https://kit-free.fontawesome.com/releases/latest/css/free.min.css',
      },
      { rel: 'preconnect', href: 'https://fonts.googleapis.com' },
      { rel: 'preconnect', href: 'https://fonts.gstatic.com', crossorigin: '' },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@400;700&display=swap',
      },
    ],
  },

  css: ['~/assets/scss/main.scss'],

  plugins: [{ src: '~/plugins/bootstrap.js', ssr: false }],

  components: true,

  buildModules: ['@nuxtjs/eslint-module'],

  modules: ['@nuxtjs/axios', '@nuxtjs/pwa'],

  axios: {},

  pwa: {
    manifest: {
      lang: 'en',
      name: process.env.npm_package_name || '',
      short_name: process.env.npm_package_name || '',
      title: process.env.npm_package_name || '',
      description: process.env.npm_package_description || '',
      'og:title': process.env.npm_package_name || '',
      'og:description': process.env.npm_package_description || '',
      theme_color: '#039be5',
      background_color: '#fff',
      display: 'fullscreen',
    },
  },

  build: {
    babel: {
      plugins: [['@babel/plugin-proposal-private-methods', { loose: true }]],
    },
    loaders: {
      scss: {
        implementation: Sass,
        sassOptions: {
          fiber: Fiber,
        },
      },
    },
  },
}
